<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $string = strtolower(preg_replace("/[^A-Za-z]/", "", file_get_contents($filePath)));

        return $string;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $occurrencesArray = count_chars($parsedFile, 1);

        asort($occurrencesArray);
        $keys = array_keys($occurrencesArray);

        $median = round(count($occurrencesArray)/2) - 1;
        $key = $keys[$median];
        $occurrences = $occurrencesArray[$key];

        return chr($key);
    }
}