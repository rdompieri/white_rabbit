<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        $coins = array(
            '1'   => 0,
            '2'   => 0,
            '5'   => 0,
            '10'  => 0,
            '20'  => 0,
            '50'  => 0,
            '100' => 0
        );

        $keys = array_keys($coins);
        $computedAmount = $amount;
        for ($i=count($keys)-1; $i >= 0 && $computedAmount; $i--) { 
            if ((int)$keys[$i] > $computedAmount) {
                continue;
            }

            $division  = floor($computedAmount / (int)$keys[$i]);
            $rest      = $computedAmount % (int)$keys[$i];
            $coins[$keys[$i]] = $division;

            $computedAmount = $rest;
        }

        return $coins;
    }
}